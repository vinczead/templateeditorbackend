const express = require("express");
const path = require("path");
const app = express();
const port = 3000;
const cache = require("memory-cache");
const fetch = require("node-fetch");

var { Liquid } = require("liquidjs");
var engine = new Liquid({
    root: path.resolve(__dirname, "template"),
    extname: ".liquid"
});

//This map should be editable and is stored somewhere
const routeFileMap = {
    "/": "home",
    "/products": "products",
    "/about": "about"
}

var cacheMiddleware = (duration) => {
    return (req, res, next) => {
        const key = "__express__" + req.originalUrl || req.url;
        const cachedContent = cache.get(key);

        if(!!cachedContent) {
            console.log("Products served from cache.");
            res.locals.products = cachedContent;
            return next();
        } else {
            const page = req.query["page"] || 0;
            const size = req.query["size"] || 5;
            //should use paginated version instead
            //should get products based on country
            fetch("https://api.dev.monotik.straxus.hu/api/channel/products?countryCode=HU")
            .then(res => res.json())
            .then(json => {
                const products = json.items;
                const paginatedProducts = products.slice(page*size, page*size+size);
                console.log("Products fetched from API.");
                cache.put(key, paginatedProducts, duration * 1000);
                res.locals.products = paginatedProducts;
                return next();
            });
            
        }
    }
}

app.get("/products", cacheMiddleware(20), async (req, res) => {
    console.log(req.path);

    const page = req.query["page"] || 0;
    const size = req.query["size"] || 5;

    const content = await engine.renderFile(routeFileMap[req.path.toString()], {products: res.locals.products, page, size});

    const renderedTemplate = await engine.renderFile("template", {content });
    res.send(renderedTemplate);
});

app.get("/*", async (req, res) => {
    console.log(req.path);
    const content = await engine.renderFile(routeFileMap[req.path.toString()]);

    const renderedTemplate = await engine.renderFile("template", {content});
    res.send(renderedTemplate);
})

app.listen(port, () => {
    console.log(`Template Editor Prototype backend @ http://localhost:${port}`);
})
